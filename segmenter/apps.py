from django.apps import AppConfig


class SegmenterConfig(AppConfig):
    name = 'segmenter'
