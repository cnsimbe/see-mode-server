from vmtk import vmtkscripts


def segmentImage(fileName, segmentValue, outputFilePath):

	# Read image
	vmtkimagereader = vmtkscripts.vmtkImageReader()
	vmtkimagereader.InputFileName = fileName
	vmtkimagereader.Execute()

	# Level set segmentation: isosurface
	vmtkimageinitialization = vmtkscripts.vmtkImageInitialization()
	vmtkimageinitialization.Image = vmtkimagereader.Image
	vmtkimageinitialization.Interactive = 0
	vmtkimageinitialization.Method = 'isosurface'
	isosurfacevalue = segmentValue
	vmtkimageinitialization.IsoSurfaceValue = float(isosurfacevalue)
	vmtkimageinitialization.Execute()

	vmtklevelsetsegmentation = vmtkscripts.vmtkLevelSetSegmentation()
	vmtklevelsetsegmentation.Image = vmtkimagereader.Image
	vmtklevelsetsegmentation.InitializationImage = vmtkimageinitialization.InitialLevelSets
	vmtklevelsetsegmentation.InitialLevelSets = vmtkimageinitialization.InitialLevelSets
	vmtklevelsetsegmentation.NumberOfIterations = 100
	vmtklevelsetsegmentation.Execute()

	# Marching cube surface generation
	vmtkmarchingcubes = vmtkscripts.vmtkMarchingCubes()
	vmtkmarchingcubes.Image = vmtklevelsetsegmentation.LevelSets
	vmtkmarchingcubes.Execute()

	# Surface smoothing
	vmtksurfacesmoothing = vmtkscripts.vmtkSurfaceSmoothing()
	vmtksurfacesmoothing.Surface = vmtkmarchingcubes.Surface
	vmtksurfacesmoothing.NumberOfIterations = 100
	vmtksurfacesmoothing.PassBand = 0.1
	vmtksurfacesmoothing.Execute()

	vmtksurfacewriter = vmtkscripts.vmtkSurfaceWriter()
	vmtksurfacewriter.Surface = vmtksurfacesmoothing.Surface
	vmtksurfacewriter.OutputFileName = outputFilePath
	vmtksurfacewriter.Execute()