from django.urls import path,re_path
from .views import SegmentView
from django.apps import apps

urlpatterns = [
	re_path(r'^$', SegmentView.as_view(),name='segment-view'),
]