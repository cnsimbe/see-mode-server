from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, FileResponse
from django.views import View
import json
import tempfile


from .seg import segmentImage

# Create your views here.

class SegmentView(View):


	def post(self, request): # create news feed item as well
		
		imageFile = request.FILES['image'];
		segmentValue = int(request.POST.get('segmentValue'))
		imageFormat = request.POST.get('imageFormat')
		outFile = tempfile.NamedTemporaryFile(delete=False,suffix=".vtp")

		inFile = tempfile.NamedTemporaryFile(suffix="."+imageFormat)

		for chunk in imageFile.chunks():
			inFile.write(chunk)

		try:

			segmentImage(inFile.name,segmentValue,outFile.name)

			outFile.delete = True
			inFile.close()
			return FileResponse(open(outFile.name, 'rb'))

		except Exception as exp:
			return HttpResponse("Error processing image",status=500)