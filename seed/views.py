from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, FileResponse
from django.views import View
import json
import tempfile


from .seedBasedSeg import seedImage

# Create your views here.

class SeedView(View):


	def post(self, request): # create news feed item as well
		
		imageFile = request.FILES['image'];
		imageFormat = request.POST.get('imageFormat')
		seed1 = json.loads(request.POST.get('seed1'))
		seed2 = json.loads(request.POST.get('seed2'))

		seed1[0] = [int(i) for i in seed1[0]]
		seed1[1] = int(seed1[1])

		seed2[0] = [int(i) for i in seed2[0]]
		seed2[1] = int(seed2[1])

		outFile = tempfile.NamedTemporaryFile(delete=False,suffix=".vtp")

		inFile = tempfile.NamedTemporaryFile(suffix="."+imageFormat)

		for chunk in imageFile.chunks():
			inFile.write(chunk)

		try:
			seedImage(inFile.name, seed1, seed2 ,outFile.name)
			outFile.delete = True
			inFile.close()
			return FileResponse(open(outFile.name, 'rb'))

		except Exception as exp:
			return HttpResponse("Error processing image",status=500)