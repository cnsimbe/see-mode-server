from django.urls import path,re_path
from .views import SeedView
from django.apps import apps

urlpatterns = [
	re_path(r'^$', SeedView.as_view(),name='seed-view'),
]