# SeeModeServer

This project was generated with Django 2.0.5 and Python 3.6.1

## Installation

Please install the CORS Headers package `pip install django-cors-headers` 

Requies `python ~ 3.6`, `django ~ 2.0.5`, `pip ~ 10.0.1` and `vmtk 1.4`


## Development server

Run `python manage.py runserver` to start the development server.
Make sure the server is running on `localhost` and port `8000` because the web app is configured to communicate with `localhost:8000`
